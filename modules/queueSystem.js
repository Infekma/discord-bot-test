
class QueueSystem {
    peopleInQueue = new Array();

    constructor()  { 
        console.log("Queue System Up");
    }

    findUser(user) {
        for(var i = 0; i < this.peopleInQueue.length; ++i) {
            if(this.peopleInQueue[i].ID == user.id &&
                this.peopleInQueue[i].name == user.username) 
            {
                return this.peopleInQueue[i];
            }
        }
        return false;
    }

    joinQueue(user) {
        var idInList = this.peopleInQueue.push(
            { 
                ID: user.id, 
                name: user.username, 
                posInQueue : this.peopleInQueue.length + 1
            });
        return this.peopleInQueue[idInList-1];
    }
    
    leaveQueue(user) {
        var queueUser = this.findUser(user);

        if(queueUser) {
            var userPosInQueue = queueUser.posInQueue-1;
            this.peopleInQueue.splice(userPosInQueue, 1);

            for(var i = userPosInQueue; i < this.peopleInQueue.length; ++i) {
                var people = this.peopleInQueue[i];
                people.posInQueue = i + 1;
            }

            return true;
        } else {
            return false;
        }
    }

    getSize() {
        return this.peopleInQueue.length;
    }
}
 
module.exports.QueueSystem = new QueueSystem(); 
