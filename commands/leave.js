const { Command } = require('discord-akairo');

var { QueueSystem } = require('../modules/queueSystem.js');

class JoinQueueCommand extends Command {
    constructor() {
        super('leave', {
           aliases: ['leave'] 
        });
    }

    exec(message) {
        const user = message.author;
        var queueUser = QueueSystem.findUser(user);
        if (!queueUser) {
            return message.reply('You were not in the queue to start with!');
        } else {
            QueueSystem.leaveQueue(user);
            return message.reply("You have left the queue!");
        }
    }
}
module.exports = JoinQueueCommand;