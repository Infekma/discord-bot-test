const { Command } = require('discord-akairo');

const { QueueSystem } = require('../modules/queueSystem.js');


class JoinQueueCommand extends Command {
    constructor() {
        super('pos', {
           aliases: ['pos'] 
        });
    }

    exec(message) {
        const queueUser = QueueSystem.findUser(message.author);
        if (queueUser) {
            return message.reply('Position in queue: **' + queueUser.posInQueue+ '**')
        } else {
            return message.reply("You are not in the queue!");
        }
    } 
}
module.exports = JoinQueueCommand; 