const { Command } = require('discord-akairo');

var { QueueSystem } = require('../modules/queueSystem.js');

class JoinQueueCommand extends Command {
    constructor() {
        super('join', {
           aliases: ['join'] 
        });
    }

    exec(message) {
        const user = message.author;
        var queueUser = QueueSystem.findUser(user);
        if (!queueUser) {
            queueUser = QueueSystem.joinQueue(user);
            message.reply('You have been added to the queue!');
            return message.reply('Position in queue: **' + queueUser.posInQueue+ '**');
        } else {
            return message.reply('You are already in the queue!');
        }
    }
}
module.exports = JoinQueueCommand;