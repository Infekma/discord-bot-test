const { Command } = require('discord-akairo');

var { QueueSystem } = require('../modules/queueSystem.js');

class JoinQueueCommand extends Command {
    constructor() {
        super('list', {
           aliases: ['list'] 
        });
    }

    exec(message) {
        const user = message.author;
        
        if (QueueSystem.getSize() > 0 ) {
            for(var i = 0; i < QueueSystem.getSize(); ++i){
                var person = QueueSystem.peopleInQueue[i];
                message.reply(person.name + " is at position " + person.posInQueue);
            } 
        } else {
            return message.reply("The queue is empty :(");
        }
    }
}
module.exports = JoinQueueCommand;